define([
  "dojo/_base/declare",
  "dojo/_base/lang",
  "dojo/_base/array",
  "dojo/Deferred",
  "dojo/promise/all",
  "esri/request",
  "esri/geometry/webMercatorUtils",
  "elevationTools/LRUTileCache",
  "elevationTools/LercDecoder",
], function (declare, lang, array, Deferred, all, esriRequest, webMercatorUtils, LRUTileCache, Lerc) {
  return declare([], {
    declaredClass: "elevationTools.ElevationQuery",
    url: null,
    noDataValue: null,
    demResolution: null,

    constructor: function (options) {
      this._options = options || {};
      this.url = this._options.url;
      this.noDataValue = this._options.noDataValue !== undefined ? this._options.noDataValue : -9999.0;
      this.demResolution = this._options.demResolution !== undefined ? this._options.demResolution : "max";

      this._isLoaded = false;
      this._tileCache = new LRUTileCache();
      this._tileMapping = {};
    },

    load: function () {
      var dfd = new Deferred();

      var urlWithJson = this.url;
      if (urlWithJson.includes("?")) {
        urlWithJson = urlWithJson + "&f=json";
      } else {
        urlWithJson = urlWithJson + "?f=json";
      }

      fetch(urlWithJson).then(
        lang.hitch(this, function (response) {
          if (response.status !== 200) {
            dfd.reject("Unsupported elevation layer");
            return dfd;
          }

          response.json().then(
            lang.hitch(this, function (result) {
              this._layerInfo = result;
              if (
                this._layerInfo.cacheType &&
                this._layerInfo.cacheType === "Elevation" &&
                this._layerInfo.pixelType &&
                this._layerInfo.pixelType === "F32" &&
                this._layerInfo.bandCount &&
                this._layerInfo.bandCount === 1 &&
                this._layerInfo.tileInfo &&
                this._layerInfo.tileInfo.format &&
                this._layerInfo.tileInfo.format === "LERC"
              ) {
                this._isLoaded = true;
                this._setInitialZoomLevel();
                dfd.resolve();
              } else {
                dfd.reject("Unsupported elevation layer");
              }
            }),
            function (error) {
              dfd.reject(error);
            }
          );
        }),
        function (error) {
          dfd.reject(error);
        }
      );

      return dfd;
    },

    queryElevation: function (longitude, latitude) {
      var dfd = new Deferred();
      if (!this._isLoaded) {
        dfd.reject("Elevation layer is not loaded !");
      } else {
        this._getTileData(longitude, latitude, this.initialZoomLevel, this._layerInfo.tileInfo).then(
          lang.hitch(this, function (tileData) {
            var z = this._sampleData(longitude, latitude, tileData);
            dfd.resolve(z);
          }),
          lang.hitch(this, function () {
            dfd.resolve(this.noDataValue);
          })
        );
      }

      return dfd;
    },

    queryElevations: async function (points) {
      var dfd = new Deferred();
      if (!this._isLoaded) {
        dfd.reject("Elevation layer is not loaded !");
      } else {
        var results = [];

        for (var i = 0; i < points.length; ++i) {
          try {
            var result = await this.queryElevation(points[i][0], points[i][1]);
            results[i] = result;
          } catch {
            results[i] = this.noDataValue;
          }
        }

        dfd.resolve(results);
      }

      return dfd;
    },

    _setInitialZoomLevel: function () {
      var layerTileInfo = this._layerInfo.tileInfo;

      if (typeof this.demResolution === "number") {
        if (this.demResolution < layerTileInfo.lods[layerTileInfo.lods.length - 1].resolution) {
          this.initialZoomLevel = layerTileInfo.lods.length - 1;
        } else if (this.demResolution > layerTileInfo.lods[0].resolution) {
          this.initialZoomLevel = 0;
        } else {
          var res1, res2;
          for (var i = layerTileInfo.lods.length - 1; i > 0; --i) {
            res1 = layerTileInfo.lods[i].resolution;
            res2 = layerTileInfo.lods[i - 1].resolution;
            if (this.demResolution === res1) {
              this.initialZoomLevel = i;
            } else if (this.demResolution === res2) {
              this.initialZoomLevel = i - 1;
            } else if (this.demResolution > res1 && this.demResolution < res2) {
              if (Math.abs(this.demResolution - res1) < Math.abs(this.demResolution - res2)) {
                this.initialZoomLevel = i;
              } else {
                this.initialZoomLevel = i - 1;
              }
            }
          }

          // it shouldn't happen
          if (this.initialZoomLevel === undefined) {
            this.initialZoomLevel = layerTileInfo.lods.length - 1;
          }
        }
      } else {
        this.initialZoomLevel = layerTileInfo.lods.length - 1;
      }

      console.log(this.initialZoomLevel);
    },

    _getTileData: function (longitude, latitude, zoomLevel, layerTileInfo, rootKey) {
      var dfd = new Deferred();
      var tileId = this._lngLatToTileId(longitude, latitude, zoomLevel, layerTileInfo);
      var tileKey = this._getTileKey(tileId);

      // root key
      if (!rootKey) {
        rootKey = tileKey;
      }

      // check for tile mapping
      if (rootKey && rootKey in this._tileMapping) {
        var tileData = this._tileCache.read(this._tileMapping[rootKey]);
        if (tileData) {
          dfd.resolve(tileData);
          return dfd;
        } else {
          // tile could have been removed from the cache, so remove mapping
          delete this._tileMapping[rootKey];
        }
      }

      // retrieve data for the cache (if exists)
      var tileData = this._tileCache.read(tileKey);
      if (tileData) {
        dfd.resolve(tileData);
      } else {
        // check if tile exists
        this._tileHasData(zoomLevel, tileKey).then(
          lang.hitch(this, function () {
            this._retrieveTileData(layerTileInfo, zoomLevel, tileId, tileKey, rootKey).then(
              lang.hitch(this, function (tileData) {
                // add data to cache (size in MB)
                if (!this._tileCache.hasKey(tileKey)) {
                  this._tileCache.write(tileKey, tileData, tileData.pixelData.width * tileData.pixelData.height * 1e-6);
                }

                // tile mapping
                if (!(rootKey in this._tileMapping)) {
                  this._tileMapping[rootKey] = tileKey;
                }

                dfd.resolve(tileData);
              }),
              function () {
                dfd.reject();
              }
            );
          }),
          lang.hitch(this, function () {
            if (zoomLevel !== 0) {
              // should check for the previous zoom level
              this._getTileData(longitude, latitude, --zoomLevel, layerTileInfo, rootKey).then(
                lang.hitch(this, function (tileData) {
                  dfd.resolve(tileData);
                }),
                function () {
                  dfd.reject();
                }
              );
            } else {
              // all zoom levels has been checked
              dfd.reject();
            }
          })
        );
      }

      return dfd;
    },

    _tileHasData: function (zoomLevel, tileKey) {
      var dfd = new Deferred();
      var tileMapUrl = this.url;

      if (tileMapUrl[tileMapUrl.length - 1] !== "/") {
        tileMapUrl = tileMapUrl + "/";
      }

      tileMapUrl = `${tileMapUrl}tilemap/${tileKey}/8/8&f=json`;

      fetch(tileMapUrl).then(
        function (response) {
          if (response.status !== 200) {
            dfd.reject();
            return dfd;
          }

          response.json().then(
            function (result) {
              if (result.valid && result.data && result.data[0] === 1) {
                dfd.resolve();
              } else {
                dfd.reject();
              }
            },
            function () {
              dfd.reject();
            }
          );
        },
        function () {
          dfd.reject();
        }
      );

      return dfd;
    },

    _retrieveTileData: function (layerTileInfo, zoomLevel, tileId, tileKey, rootKey) {
      var dfd = new Deferred();
      var tileUrl = this.url;

      // check for mapping using rootKey
      if (rootKey && rootKey in this._tileMapping) {
        var tileData = this._tileCache.read(this._tileMapping[rootKey]);
        if (tileData) {
          dfd.resolve(tileData);
          return dfd;
        } else {
          // tile could have been removed from the cache, so remove mapping
          delete this._tileMapping[rootKey];
        }
      }

      // check for mapping using tileKey
      if (tileKey in this._tileMapping) {
        var tileData = this._tileCache.read(this._tileMapping[tileKey]);
        if (tileData) {
          dfd.resolve(tileData);
          return dfd;
        } else {
          // tile could have been removed from the cache, so remove mapping
          delete this._tileMapping[tileKey];
        }
      }

      // check from cache
      var tileData = this._tileCache.read(tileKey);
      if (tileData) {
        dfd.resolve(tileData);
        return dfd;
      }

      if (tileUrl[tileUrl.length - 1] !== "/") {
        tileUrl = tileUrl + "/";
      }

      tileUrl = `${tileUrl}tile/${tileKey}`;

      fetch(tileUrl).then(
        function (response) {
          if (response.status !== 200) {
            dfd.reject();
            return dfd;
          }
          response.arrayBuffer().then(
            function (arrayBufferResult) {
              try {
                var pixelBlock = Lerc.decode(arrayBufferResult);

                var resolution = layerTileInfo.lods[zoomLevel].resolution;

                var w = resolution * layerTileInfo.cols;
                var h = resolution * layerTileInfo.rows;
                var xmin = layerTileInfo.origin.x + tileId.column * w;
                var ymin = layerTileInfo.origin.y - (tileId.row + 1) * h;
                var xmax = xmin + w;
                var ymax = ymin + h;

                var safeWidth = 0.99999999 * (pixelBlock.width - 1);

                var dx = (pixelBlock.width - 1) / (xmax - xmin);
                var dy = (pixelBlock.width - 1) / (ymax - ymin);

                var tileData = {
                  pixelData: pixelBlock,
                  xmin: xmin,
                  ymin: ymin,
                  xmax: xmax,
                  ymax: ymax,
                  safeWidth: safeWidth,
                  dx: dx,
                  dy: dy,
                };

                dfd.resolve(tileData);
              } catch {
                dfd.reject();
              }
            },
            function () {
              dfd.reject();
            }
          );
        },
        function () {
          dfd.reject();
        }
      );

      return dfd;
    },

    _lngLatToPixels: function (longitude, latitude, layerTileInfo) {
      var siny = Math.sin((latitude * Math.PI) / 180.0);
      siny = Math.min(Math.max(siny, -0.9999), 0.9999);
      return {
        x: layerTileInfo.cols * (0.5 + longitude / 360),
        y: layerTileInfo.rows * (0.5 - Math.log((1.0 + siny) / (1.0 - siny)) / (4.0 * Math.PI)),
      };
    },

    _lngLatToTileId: function (longitude, latitude, zoomLevel, layerTileInfo) {
      var worldPt = this._lngLatToPixels(longitude, latitude, layerTileInfo);
      var scale = 1 << zoomLevel;
      return {
        level: zoomLevel,
        column: Math.floor((worldPt.x * scale) / layerTileInfo.cols),
        row: Math.floor((worldPt.y * scale) / layerTileInfo.rows),
      };
    },

    _getTileKey: function (tileId) {
      return `${tileId.level}/${tileId.row}/${tileId.column}`;
    },

    _sampleData: function (longitude, latitude, sampleData) {
      var wmPt = webMercatorUtils.lngLatToXY(longitude, latitude);

      var x = sampleData.dx * (wmPt[0] - sampleData.xmin);
      var y = sampleData.dy * (sampleData.ymax - wmPt[1]);

      x = x < 0 ? 0 : x > sampleData.safeWidth ? sampleData.safeWidth : x;
      y = y < 0 ? 0 : y > sampleData.safeWidth ? sampleData.safeWidth : y;

      var fx = Math.floor(x);
      var fy = Math.floor(y);

      var index1 = fy * sampleData.pixelData.width + fx;
      var index2 = index1 + sampleData.pixelData.width; // next line neighbour

      var val1 = sampleData.pixelData.pixels[0][index1];
      var val2 = sampleData.pixelData.pixels[0][index2];
      var val3 = sampleData.pixelData.pixels[0][index1 + 1];
      var val4 = sampleData.pixelData.pixels[0][index2 + 1];

      var z;

      if (val1 !== this.noDataValue && val2 !== this.noDataValue && val3 !== this.noDataValue && val4 !== this.noDataValue) {
        // bilinear interpolation
        var temp1 = x - fx;
        var temp2 = val1 + (val3 - val1) * temp1;
        z = temp2 + (val2 + (val4 - val2) * temp1 - temp2) * (y - fy);
      } else {
        z = this.noDataValue;
      }

      return z;
    },
  });
});
