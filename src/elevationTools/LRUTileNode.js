define(["dojo/_base/declare"], function (declare) {
  return declare([], {
    declaredClass: "elevationTools.LRUTileNode",
    key: null,
    value: null,
    size: null,
    next: null,
    previous: null,

    constructor: function (key, value, size, next = null, prev = null) {
      this.key = key;
      this.value = value;
      this.size = size;
      this.next = next;
      this.prev = prev;
    },
  });
});
