define(["dojo/_base/declare", "elevationTools/LRUTileNode"], function (declare, LRUTileNode) {
  return declare([], {
    declaredClass: "elevationTools.LRUTileCache",
    size: 0,
    limit: 512, // limit in MB
    head: null,
    tail: null,
    cache: {},

    constructor: function (limit = 512) {
      this.size = 0;
      this.limit = limit;
      this.head = null;
      this.tail = null;
      this.cache = {};
    },

    hasKey: function (key) {
      return key in this.cache;
    },

    write: function (key, value, size) {
      this._checkLimit();

      if (!this.head) {
        this.head = this.tail = new LRUTileNode(key, value, size);
      } else {
        var node = new LRUTileNode(key, value, size, this.head);
        this.head.prev = node;
        this.head = node;
      }

      this.cache[key] = this.head;
      this.size += size;
    },

    read: function (key) {
      if (key in this.cache) {
        var value = this.cache[key].value;
        var size = this.cache[key].size;

        this.remove(key);
        this.write(key, value, size);

        return value;
      }
    },

    remove: function (key) {
      var node = this.cache[key];

      if (node.prev !== null) {
        node.prev.next = node.next;
      } else {
        this.head = node.next;
      }

      if (node.next !== null) {
        node.next.prev = node.prev;
      } else {
        this.tail = node.prev;
      }

      delete this.cache[key];
      this.size -= node.size;
    },

    clear: function () {
      this.head = null;
      this.tail = null;
      this.size = 0;
      this.cache = {};
    },

    _checkLimit: function () {
      if (this.size >= this.limit) {
        this.remove(this.tail.key);
      }
    },
  });
});
